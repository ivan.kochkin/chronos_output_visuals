## How to use.
0. Do it once! Launch install.bat
1. If you using a single chronos_battery_out.csv, then place it in the same directory as ChronosOutputPlots.py
2. If you using multi-years Chronos outputs, then place them to the chronos_output folder in the same directory. Make sure that you preserve initial structure (YYYY/chronos_battery_out.csv).
3. Launch run.bat
