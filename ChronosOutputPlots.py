from dash import Dash, html, dcc
from dash.dependencies import Input, Output, State
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
import random

app = Dash(__name__)


def give_me_plot(in_data):
    if in_data is None:
        return go.Figure()
    line_left, line_right, bar_right, data, index_range = in_data
    # Create figure with secondary y-axis
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    if index_range:
        data = data.iloc[index_range[0]:index_range[1]+1, :]
    # Add traces
    for col in line_left:
        fig.add_trace(
            go.Scatter(x=data.index, y=data[col], name=col, line={'dash': 'dash'}),
            secondary_y=False,
        )

    for col in line_right:
        fig.add_trace(
            go.Scatter(x=data.index, y=data[col], name=col),
            secondary_y=True,
        )

    for col in bar_right:
        fig.add_trace(
            go.Bar(x=data.index, y=data[col], name=col),
            secondary_y=True,
        )

    # Add figure title
    fig.update_layout(
        barmode='relative',
        title_text="Chronos Dispatch Example",
        height=800
    )

    # Set x-axis title
    fig.update_xaxes(title_text="Time")

    # Set y-axes titles
    fig.update_yaxes(title_text="Price", secondary_y=False)
    fig.update_yaxes(title_text="Dispatch / Capacity reserve", secondary_y=True)

    return fig


def give_me_data_for_plot():

    try:
        df = pd.read_csv('chronos_battery_out.csv').fillna(0)
    except:
        return None
    for column in df.columns:
        if 'import' in column and 'price' not in column and min(df[column]) < 0:
            df[column] = -df[column]
        if 'export' in column and 'price' not in column and max(df[column]) > 0:
            df[column] = -df[column]

    line_left = [
        'dayahead_import_price_currencypermwh',
        'aFRR_energy_import_price_currencypermwh',
        'aFRR_capacity_import_capacity_price_currencypermwperh',
        'aFRR_capacity_export_capacity_price_currencypermwperh',
        'fcr_import_capacity_price_currencypermwperh',
        'fcr_export_capacity_price_currencypermwperh',
    ]
    line_right = [
        'duration_h',
        'aFRR_capacity_import_fulfilled_capacity_h',
        'aFRR_capacity_export_fulfilled_capacity_h',
        'fcr_export_fulfilled_capacity_h',
        'fcr_import_fulfilled_capacity_h',
    ]

    bar_right = [
        'dayahead_import_flh',
        'dayahead_export_flh',
        'aFRR_energy_import_flh',
        'aFRR_energy_export_flh',
        'fcr_import_flh',
        'fcr_export_flh'
    ]
    return line_left, line_right, bar_right, df, None


# give_me_plot(give_me_data_for_plot())


def multy_years_plots(years=(2026, 2040)):
    years = range(years[0], years[1]+1)
    dfs = {}

    failed_years = []
    for year in years:
        try:
            dfs[year] = pd.read_csv(f'./chronos_output/{year}/chronos_battery_out.csv').fillna(0)
        except:
            failed_years.append(year)
    years = [year for year in years if year not in failed_years]
    if len(years) == 0:
        return go.Figure()
    mask = '_gross_margin_currencyperkw'
    markets = [
        column[:-len(mask)]
        for column in dfs[years[0]].columns
        if mask in column
        and 'wholesale' not in column
        and 'arbitrage' not in column
    ]
    columns_to_count = {
        market: [
            column
            for column in dfs[years[0]].columns
            if market in column
            and (
                       'fulfilled_capacity_h' in column
                       or 'flh' in column
               )
            and 'allocated' not in column
        ]
        for market in markets
    }
    for market in markets:
        for year in years:
            dfs[year][f'{market}_active'] = (sum(dfs[year][column] ** 2 for column in columns_to_count[market]) > 0) * 1
    fig = go.Figure(data=[
        go.Bar(name=market, x=list(years), y=[sum(dfs[year][f'{market}_active']) for year in years])
        for market in markets
    ])
    # Change the bar mode
    fig.update_layout(barmode='stack')
    yield fig

    costs = -600
    rate = 0.06
    fig = go.Figure(go.Waterfall(
        x=['costs'] + markets + ['NPV'],
        measure=["absolute"] + ["relative" for _ in markets] + ["total"],
        y=[costs] + [sum(sum(dfs[year][f'{market}{mask}']) * (1+rate)**(min(years) - year) for year in years) for market in markets] + [None],
    ))

    fig.update_layout(title="Profit and loss statement", waterfallgap=0.3)

    yield fig


app.layout = html.Div(
    children=[
        dcc.Graph(figure=fig) for fig in multy_years_plots()
    ] + [dcc.Graph(figure=give_me_plot(give_me_data_for_plot()))]
)

if __name__ == '__main__':
    app.run_server(debug=True)
